
# Description
This is a very simple utility to control PTZ features on very cheap cameras when you don't want to install, for some reasons, vendor's app or software.

It comes handy also when you want to control your camera PTZ from an external program (in my case from Shinobi CCTV https://shinobi.video/)

Basicly it proxies the request to your camera to perform the comand.

Here some curl examples :

curl -i "http://camerauser:camerapassword@programIP:programPort/sriCam.camSP012/192.168.1.16:5000/up"

curl -i "http://camerauser:camerapassword@programIP:programPort/sriCam.camSP012/192.168.1.16:5000/down"

curl -i "http://camerauser:camerapassword@programIP:programPort/sriCam.camSP012/192.168.1.16:5000/right"

curl -i "http://camerauser:camerapassword@programIP:programPort/sriCam.camSP012/192.168.1.16:5000/left"


This program has no security at all so you should use it only on loopback network or in a very trusted and isolated network.

## request path explained:

producer.camera/camraIP:port/comand

supported command (ptz movements):
up, down, right and left

### producers and cameras are defined here:

CameraDefinitions/

At this moment the only supported camera is just Sricam SP012 (it's the one that I own). It's kind a ONVIF camera. 

if you use the DockerFile the defaul port is 80.

docker build -t pyptzimg .

docker run -d --name pyptz1 pyptzimg


If you want to add more cameras or functions feel free to clone or push changes on this project.  

