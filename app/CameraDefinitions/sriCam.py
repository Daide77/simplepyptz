from   CameraDefinitions.generalCamera import generalCamera
import requests
import sys

class camSP012(generalCamera):
    def __init__(self, ip='192.168.X.XX', port='5000', user='XXXXXX', passwd='XXXXXX'):
        self.XMAX = 1
        self.XMIN = -1
        self.YMAX = 1
        self.YMIN = -1
        self.ptz_body = """<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:wsdl="http://www.onvif.org/ver20/ptz/wsdl" xmlns:sch="http://www.onvif.org/ver10/schema">
						   <soap:Header/>
							   <soap:Body>
									<wsdl:ContinuousMove>
										<wsdl:Velocity>
											<!--Optional:-->
											<sch:PanTilt x="{}" y="{}" space="{}"/>
										</wsdl:Velocity>
										<!--Optional:-->
									</wsdl:ContinuousMove>
								</soap:Body>
							</soap:Envelope>"""
        self.headers = {'content-type': 'text/xml', 'passwd': passwd, 'port': port, 'user': user}
        self.url = 'http://{}:{}/'.format(ip, port)

    def move_right(self,  timeout=2):
        x = 0
        y = self.YMAX
        response = requests.post(self.url, data=self.ptz_body.format(x, y, '0'), headers=self.headers)
        return response.status_code

    def move_left(self,  timeout=2):
        x = 0
        y = self.YMIN
        response = requests.post(self.url, data=self.ptz_body.format(x, y, '0'), headers=self.headers)
        return response.status_code

    def move_up(self,  timeout=2):
        x = self.XMAX
        y = 0
        response = requests.post(self.url, data=self.ptz_body.format(x, y, '0'), headers=self.headers)
        return response.status_code

    def move_down(self,  timeout=2):
        x = self.XMIN
        y = 0
        response = requests.post(self.url, data=self.ptz_body.format(x, y, '0'), headers=self.headers)
        return response.status_code

