from flask             import Flask, request
from CameraDefinitions import *

app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def hello_world(path):
    # print( "headers", request.headers )
    fake, camType, hostPort, cmd = (request.path).split("/") 
    host, port                   = hostPort.split(":") 
    try:
      MyClass                    = eval(camType)
    except AttributeError:
      return f"Sorry camera not found in driver [{camType}] ", 404
    except NameError:
      return f"Sorry driver not found [{camType}] "          , 404
    except Exception as e:
      return f"Error on driver call {type(e).__name__}"      , 404 
    instance                     = MyClass( host,port,request.authorization["username"],request.authorization["password"] )
    if cmd == "up":
        response                 = instance.move_up()
        return f"Sent [{cmd}] ", response
    elif cmd == "down":
        response                 = instance.move_down()
        return f"Sent [{cmd}]", response
    elif cmd == "right":
        response                 = instance.move_left()
        return f"Sent cmd [{cmd}] ", response
    elif cmd == "left":
        response                 = instance.move_right()
        return f"Sent cmd [{cmd}] ", response
    else:
        return f"Sorry bud I can't [{cmd}] ", 404

if __name__ == '__main__':
    print('Starting app')
    app.run(host='0.0.0.0', debug=False, port=8080)
